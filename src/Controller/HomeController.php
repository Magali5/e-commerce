<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use App\Entity\Product;

class HomeController extends AbstractController
{
  /**
   * @Route ("/home", name="home")
   */
  public function index()
  {
    $repo = $this->getDoctrine()->getRepository(Product::class);

    $product = $repo->findAll();

    return $this->render('home.html.twig', [
      'product' => $product,

    ]);
  }
}

