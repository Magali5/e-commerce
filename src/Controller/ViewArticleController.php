<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ProductRepository;
use App\Entity\Product;

class ViewArticleController extends Controller
{
    /**
     * @Route("/view/product/{id}", name="view_product")
     */
    public function index(int $id, ProductRepository $repo) {

        $repo = $this->getDoctrine()->getRepository(Product::class);

       $product = $repo->find($id);
                dump($product);
        return $this->render('view_article.html.twig', [
            //"product" => $repo->find($id),
            "product" => $product,
            //'view_article' => 'ViewArticleController',
        ]);
    }
}