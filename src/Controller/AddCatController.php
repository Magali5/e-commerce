<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AddCatController extends Controller
{
    /**
     * @Route("/add/cat", name="add_cat")
     */
    public function index()
    {
        return $this->render('add_cat/addCat.html.twig', [
            'controller_name' => 'AddCatController',
        ]);
    }
}
