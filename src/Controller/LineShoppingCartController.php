<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LineShoppingCartController extends Controller
{
    /**
     * @Route("/line/shopping/cart", name="line_shopping_cart")
     */
    public function index()
    {
        return $this->render('line_shopping_cart/index.html.twig', [
            'controller_name' => 'LineShoppingCartController',
        ]);
    }
}
